// comment out the line below before pushing to heroku
// require('dotenv').config();
require('./src/models/User');
require('./src/models/Todo');

const express = require('express');
const authRoutes = require('./src/routes/authRoutes');
const todoRoutes = require('./src/routes/todoRoutes');
const mongoose = require('mongoose');
const graphqlHTTP = require('express-graphql');
const schema = require('./src/schema/schema');

const app = express();
app.use(express.json());
app.use(authRoutes);
app.use(todoRoutes);
app.use('/graphql', graphqlHTTP({
  graphiql: true,
  schema
}));

const mongoUri = process.env.MONGODB_ADMIN;
if (!mongoUri) throw new Error('You must provide the authentication string from MongoDB.');

mongoose.connect(mongoUri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
});

mongoose.connection.on('connected', () => console.log('Connected to MongoDB cluster.'));
mongoose.connection.on('error', (err) => console.log('Error connecting to MongoDB.', err));



app.get('/', (req, res) => res.send('Welcome to your React ToDo App!'));



const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));