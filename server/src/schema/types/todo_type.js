const axios = require('axios');

// dev instance with auth headers
const go = axios.create({
  baseURL: 'https://react-todo-server-app.herokuapp.com',
  headers: {
    Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZWM4NjQwNmIwNjlhZjFkYjA3ZGM3MWEiLCJpYXQiOjE1OTA2NTMxNTZ9.hUF8BElJjPryZsKiGoD6mjYFgPTxvDQ06f-qom4yR7M'
  }
})

const {
  GraphQLObjectType,
  GraphQLList,
  GraphQLNonNull,
  GraphQLString,
  GraphQLBoolean
} = require('graphql');



const TodoType = new GraphQLObjectType({
  name: 'Todo',
  fields: () => ({
    _id:      { type: GraphQLString },
    title:    { type: GraphQLString },
    checked:  { type: GraphQLBoolean },
    starred:  { type: GraphQLBoolean },
    note:     { type: GraphQLString },
    subtasks: { 
      type: new GraphQLList(SubtaskType),
      resolve(parentValue, args) {
        return go.get(`/mytodos`)
        // finish the resolve function
      }
    }
  })
});


const SubtaskType = new GraphQLObjectType({
  name: 'Subtask',
  fields: () => ({
    _id:      { type: GraphQLString },
    title:    { type: GraphQLString },
    checked:  { type: GraphQLBoolean }
  })
});



module.exports = TodoType;