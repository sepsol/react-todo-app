const axios = require('axios');

// dev instance with auth headers
const go = axios.create({
  baseURL: 'http://localhost:3000',
  headers: {
    Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZWM4NjQwNmIwNjlhZjFkYjA3ZGM3MWEiLCJpYXQiOjE1OTA2NTMxNTZ9.hUF8BElJjPryZsKiGoD6mjYFgPTxvDQ06f-qom4yR7M'
  }
})

const { 
  GraphQLObjectType, 
  GraphQLList,
  GraphQLSchema,
  GraphQLString,
  GraphQLBoolean
} = require('graphql');

const TodoType = require('./types/todo_type');



const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    todos: {
      type: new GraphQLList(TodoType),
      resolve(parentValue, args, req) {
        return go.get('/mytodos')
          .then(res => res.data);
      }
    }
  }
});


const RootMutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addTodo: {
      type: TodoType,
      args: { title: { type: GraphQLString } },
      resolve(parentValue, args, req) {
        return go.post('/mytodos', args)
          .then(res => {console.log(res.data); return res.data.todo});
      }
    }
  }
});



module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: RootMutation
});