const mongoose = require('mongoose');
const bcrypt = require('bcrypt');



const userSchema = new mongoose.Schema({
  username: { type: String, minlength: 6, required: true, unique: true, lowercase: true },
  password: { type: String, minlength: 8, required: true }
});



userSchema.pre('save', function(next) {
  const user = this;
  if (!user.isModified('password')) return next();

  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);
    
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) return next(err);

      user.password = hash;
      next();
    });
  });
});


userSchema.methods = {
  verifyPassword: function(receivedPassword) {
    const user = this;
    
    return new Promise((resolve, reject) => {
      bcrypt.compare(receivedPassword, user.password, (err, same) => {
        if (err || !same) return reject(err);
        resolve(true);
      });
    });
  }
}



mongoose.model('User', userSchema);