const mongoose = require('mongoose');

// BUG: position should be a unique sequal of numbers
// the unique flag doesn't work properly for some reason

const subtaskSchema = new mongoose.Schema({
  // position: { type: Number, min: 1, unique: true },
  title:    String,
  checked:  Boolean
});


const todoSchema = new mongoose.Schema({
  userId:   { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  // position: { type: Number, min: 1, unique: true },
  title:    String,
  checked:  Boolean,
  starred:  Boolean,
  subtasks: [subtaskSchema],
  note:     String
});



mongoose.model('Todo', todoSchema);