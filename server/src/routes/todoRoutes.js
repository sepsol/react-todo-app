const express = require('express');
const requireAuth = require('../middlewares/requireAuth');
const mongoose = require('mongoose');

const router = express.Router();
const Todo = mongoose.model('Todo');

router.use('/mytodos', requireAuth);



router.get('/mytodos', async (req, res) => {
  const todos = await Todo.find({ userId: req.user._id });
  res.send(todos);
});


router.post('/mytodos', async (req, res) => {
  const { title } = req.body;
  if (!title) return res.status(422).send('Bad request.');
  try {
    const todo = new Todo({ userId: req.user._id, title, checked: false, starred: false, subtasks: [], note: '' });
    await todo.save();
    res.send({ todo });
  } catch (err) {
    res.status(422).send({ error: err.message });
  }
});


// checking a todo
router.put('/mytodos/:id', async (req, res) => {
  const id = req.params.id;
  const { checked } = req.body;
  if (!id) return res.status(422).send('Bad request.');
  await Todo.updateOne({ _id: id }, {$set: { checked }}, (err, result) => {
    if (err) return res.status(422).send('Bad request.');
    res.send(result);
  });
});


// editing a todo
router.put('/mytodos/:id/edit', async (req, res) => {
  const id = req.params.id;
  const { title } = req.body;
  if (!id) return res.status(422).send('Bad request.');
  await Todo.updateOne({ _id: id }, {$set: { title }}, (err, result) => {
    if (err) return res.status(422).send('Bad request.');
    res.send(result);
  })
});


router.delete('/mytodos/:id', async (req, res) => {
  const id = req.params.id;
  if (!id) return res.status(422).send('Bad request.');
  await Todo.deleteOne({ _id: id }, (err, result) => {
    if (err) res.status(422).send('Bad request.');
    res.send(result);
  });
});





module.exports = router;