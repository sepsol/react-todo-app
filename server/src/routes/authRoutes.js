const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const router = express.Router();
const User = mongoose.model('User');



router.post('/signup', async (req, res) => {
  const { username, password } = req.body;
  if (!username || !password) return res.status(422).send('You must provide a username and password.');

  try {
    const user = new User({ username, password });
    await user.save();

    const token = jwt.sign({ userId: user._id }, process.env.JWT_KEY);
    res.send({ token });

  } catch (err) {
    res.status(422).send({ error: err.message });
  }
});


router.post('/signin', async (req, res) => {
  const { username, password } = req.body;
  if (!username || !password) return res.status(422).send('You must provide your username and password.');

  const user = await User.findOne({ username });
  if (!user) return res.status(404).send('That account does not exist, sign up instead.')
  
  try {
    await user.verifyPassword(password);
    // if the promise wasn't rejected the try block will continue to execute

    const token = jwt.sign({ userId: user._id }, process.env.JWT_KEY);
    res.send({ token });
    
  } catch (err) {
    res.status(422).send({ error: 'Invalid username or password, try again.' });
  }
});



module.exports = router;