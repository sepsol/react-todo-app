const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

const User = mongoose.model('User');



function requireAuth(req, res, next) {
  const { authorization } = req.headers;
  if (!authorization) return res.status(401).send('You must sign in first.');
  const token = authorization.replace('Bearer ', '');

  jwt.verify(token, process.env.JWT_KEY, async (err, payload) => {
    if (err) return res.status(401).send('You must sign in first.');
    const { userId } = payload;
    
    const user = await User.findById(userId);
    req.user = user;

    next();
  });
}



module.exports = requireAuth;