import React, { useState, useContext } from 'react';
import { StyleSheet, Button, View, Text } from 'react-native';
import { Form, Item, Input, Label, Icon } from 'native-base';
// import FormTextInput from './FormTextInput'

import AuthContext from '../contexts/AuthContext';



function AuthForm({ title, buttonText, callback }) {
  const { authState } = useContext(AuthContext);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const userLength = username.length < 6 && username.length > 0 ? true : false;
  const passLength = password.length < 8 && password.length > 0 ? true : false;
  const itemMarginBottom = 15;

  return(
    <>
      <Text style={styles.title}>{title}</Text>
      <Form style={styles.form}>

        <Item floatingLabel 
          style={{borderColor: (userLength ? 'red' : '#999'), marginBottom: itemMarginBottom}}
        >
          <Label style={styles.label}>Username</Label>
          <Input 
            value={username}
            onChangeText={input => setUsername(input.replace(' ', ''))}
            autoCapitalize='none' 
            autoCorrect={false} 
          />
          { userLength ? <Icon name='close-circle' style={{color: 'red'}}/> : null }
        </Item>

        <Item floatingLabel 
          style={{borderColor: (passLength ? 'red' : '#999'), marginBottom: itemMarginBottom}}
        >
          <Label style={styles.label}>Password</Label>
          <Input 
            value={password}
            onChangeText={input => setPassword(input.replace(' ', ''))}
            autoCapitalize='none' 
            autoCorrect={false} 
            secureTextEntry 
          />
          { passLength ? <Icon name='close-circle' style={{color: 'red'}}/> : null }
        </Item>
      </Form>

      <Text style={styles.error}>{authState.errorMessage}</Text>

      <View style={styles.button}>
        <Button 
          title={buttonText}
          onPress={() => callback({username,password})}
        />
      </View>
    </>
  );
}


const styles = StyleSheet.create({
  form: {
    marginRight: 15,
  },
  title: {
    fontSize: 30,
    marginHorizontal: 15,
    marginBottom: 20,
    textAlign: 'center',
  },
  error: {
    marginTop: 5,
    fontSize: 16,
    marginHorizontal: 0,
    height: 35,
    color: 'red',
    textAlign: 'center',
    marginBottom: 10,
  },
  // item: {
  //   marginBottom: 20,
  // },
  label: {
    marginTop: -5,
  },
  button: {
    alignSelf: 'center',
    width: 150,
  },
});



export default AuthForm;