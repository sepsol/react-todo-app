import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';



function LinkButton({ text, callback }) {
  return(
    <TouchableOpacity onPress={callback}>
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  );
}


const styles = StyleSheet.create({
  text: {
    color: '#25f',
    textAlign: 'center'
  },
});



export default LinkButton;