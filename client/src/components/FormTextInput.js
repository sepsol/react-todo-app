import React from 'react';
import { StyleSheet,View } from 'react-native';
import { Item, Input, Label } from 'native-base';

// unsuccessful attempt at making native-base's input fields more stylish

function AuthForm() {
  return(
    <Item floatingLabel style={styles.item}>
      <Input style={styles.input}/>
      <Label style={styles.label}>Password</Label>
    </Item>
  );
}


const styles = StyleSheet.create({
  item: {
    borderColor: 'white',
  },
  input: {
    zIndex: 5,
    borderWidth: 1,
    borderColor: '#666',
    marginRight: 15,
    borderRadius: 5,
    paddingHorizontal: 10,
  },
  label: {
    zIndex: 10,
    backgroundColor: 'white',
    width: 100,
    marginLeft: 10,
  },
});



export default AuthForm;