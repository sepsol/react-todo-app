import axios from 'axios';
import { AsyncStorage } from 'react-native';



const server = axios.create({
  baseURL: "https://react-todo-server-app.herokuapp.com/"
});


server.interceptors.request.use(
  async (config) => {
    const token = await AsyncStorage.getItem('token');
    if (token) config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  (err) => Promise.reject(err)
);



export default server;