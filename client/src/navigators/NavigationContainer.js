import React, { useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AuthContext from '../contexts/AuthContext';

import AuthStackNavigator from './AuthStackNavigator';
import TodoStackNavigator from './TodoStackNavigator';



function Navigation() {
  const { authState } = useContext(AuthContext);
  
  return(
    <NavigationContainer>
      {
        authState.token === null
        ? <AuthStackNavigator/>
        : <TodoStackNavigator/>
      }
    </NavigationContainer>
  );
}




export default Navigation;