import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AuthContext from '../contexts/AuthContext';

import InitialLoadingScreen from '../screens/InitialLoadingScreen';
import SignupScreen from '../screens/SignupScreen';
import SigninScreen from '../screens/SigninScreen';



const AuthStack = createStackNavigator();

function AuthStackNavigator() {
  const { authState } = useContext(AuthContext);
  
  if (authState.isLoading === true) {
    return(
      <AuthStack.Navigator screenOptions={{headerShown: false}}>
        <AuthStack.Screen name="InitialLoadingScreen" component={InitialLoadingScreen}/>
      </AuthStack.Navigator>
    );
  } else {
    return(
      <AuthStack.Navigator screenOptions={{headerShown: false}}>
        <AuthStack.Screen name="SignupScreen" component={SignupScreen}/>
        <AuthStack.Screen name="SigninScreen" component={SigninScreen}/>
      </AuthStack.Navigator>
    );
  }
}



export default AuthStackNavigator;