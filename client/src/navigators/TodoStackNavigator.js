import React, { useContext } from 'react';
import { TouchableOpacity } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import TodoListScreen from '../screens/TodoListScreen';
import TodoDetailsScreen from '../screens/TodoDetailsScreen';

import AuthContext from '../contexts/AuthContext';
import TodoContext from '../contexts/TodoContext';
import { AntDesign } from '@expo/vector-icons'; 



const TodoStack = createStackNavigator();

function TodoStackNavigator() {
  const { signout } = useContext(AuthContext);
  const { clearTodos } = useContext(TodoContext); // for some reason this doesn't work as intended
  
  return(
    <TodoStack.Navigator 
      screenOptions={{headerRight: () => (
        <TouchableOpacity onPress={() => {clearTodos(); signout()}} style={{flexDirection: 'row'}}>
          <AntDesign name="logout" size={22} style={{marginRight: 20, transform: [{ rotate: '180deg' }] }}/>
        </TouchableOpacity>
      )}}
    >
      <TodoStack.Screen 
        name="TodoListScreen" 
        component={TodoListScreen} 
        options={{title: "My ToDo List", headerTitleAlign: 'center'}}
      />
      <TodoStack.Screen 
        name="TodoDetailsScreen" 
        component={TodoDetailsScreen}
      />
    </TodoStack.Navigator>
  );
}



export default TodoStackNavigator;