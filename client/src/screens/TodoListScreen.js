import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, Text, TextInput, FlatList, TouchableOpacity, View } from 'react-native';
import { CheckBox } from 'native-base';

import { MaterialIcons } from '@expo/vector-icons'; 
import { Ionicons } from '@expo/vector-icons'; 
import { FontAwesome } from '@expo/vector-icons'; 
import { Entypo } from '@expo/vector-icons'; 

import TodoContext from '../contexts/TodoContext';



function TodoListScreen({ navigation }) {
	const { todosState, fetchTodos, addTodo, editTodo, checkTodo, deleteTodo } = useContext(TodoContext);
	const [ isEditing, setIsEditing ] = useState(false);
	const [ addInputText, setAddInputText ] = useState('');
	const [ editInputText, setEditInputText ] = useState('');
	const [ currentTodoId, setCurrentTodoId ] = useState('');
	
  useEffect(() => {
		fetchTodos();
    const listener = navigation.addListener('focus', () => fetchTodos());
    return listener;
	});

	
  return(
		<>
			<FlatList 
				data={todosState}
				keyExtractor={item => item._id}
				renderItem={({ item }) => (
					<TouchableOpacity 
						style={styles.todoContainer} 
						// onPress={() => navigation.navigate('TodoDetailsScreen', { id: item._id })}
						onPress={() => {setIsEditing(true); setEditInputText(item.title); setCurrentTodoId(item._id)}}
					>
						<View style={styles.todoBox}>
							<CheckBox 
								checked={item.checked} 
								onPress={() => checkTodo(item._id, !item.checked)} 
								style={styles.checkbox} 
								color='#479' 
							/>
							<Text style={styles.todoText}>{item.title}</Text>
						</View>
						<TouchableOpacity onPress={() => deleteTodo(item._id)}>
							<Ionicons name="md-remove" size={28} color='#479' />
						</TouchableOpacity>
					</TouchableOpacity>
				)}
			/>
			<View style={styles.addTodoBox}>
				{ isEditing
					? <>
							<TextInput autoFocus value={editInputText} onChangeText={setEditInputText} style={styles.inputText} placeholder="Add a new todo..." />
							<TouchableOpacity onPress={() => {setEditInputText(''); setIsEditing(false); setCurrentTodoId('')}} style={styles.cancelEditButton}>
								<Entypo name="cross" size={40} color="#964" />
							</TouchableOpacity>
							<TouchableOpacity onPress={() => {editTodo(currentTodoId, editInputText); setEditInputText(''); setIsEditing(false); setCurrentTodoId('')}} style={styles.submitEditButton}>
								<FontAwesome name="check-circle" size={40} color="#469" />
							</TouchableOpacity>
						</>
					: <>
							<TextInput value={addInputText} onChangeText={setAddInputText} style={styles.inputText} placeholder="Add a new todo..." />
							<TouchableOpacity onPress={() => {addTodo(addInputText); setAddInputText('')}} style={styles.addTodoButton}>
								<MaterialIcons name="add-box" size={40} color='#469' />
							</TouchableOpacity>
						</>
				}
			</View>
		</>
	);
}


const styles = StyleSheet.create({
	todoContainer: {
		borderBottomWidth: 1,
		borderBottomColor: '#ddd',
		height: 60,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingLeft: 15,
		paddingRight: 25,
	},
	todoBox: {
		flexDirection: 'row',
		alignItems: 'center',
		width: '85%',
	},
	checkbox: {
		transform: [{ scale: 1.2 }],
	},
	todoText: {
		marginLeft: 25,
		fontSize: 18,
	},
	addTodoBox: {
		borderTopColor: '#999',
		borderTopWidth: 1,
		backgroundColor: 'white',
		position: 'absolute',
		bottom: 0,
		flexDirection: 'row',
		width: '100%',
		height: 60,
		alignItems: 'center',
	},
	inputText: {
		flex: 1,
		paddingHorizontal: 20,
		fontSize: 18,
	},
	addTodoButton: {
		marginRight: 12,
	},
	submitEditButton: {
		marginRight: 15,
	},
	cancelEditButton: {
		marginRight: 5,
	},
});



export default TodoListScreen;