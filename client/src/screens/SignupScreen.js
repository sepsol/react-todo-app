import React, { useContext, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';

import AuthContext from '../contexts/AuthContext';

import AuthForm from '../components/AuthForm';
import LinkButton from '../components/LinkButton';



function SignupScreen({ navigation }) {
  const { clearErrorMessage, signup } = useContext(AuthContext);
  
  useEffect(() => {
    const listener = navigation.addListener('blur', () => clearErrorMessage());
    return listener;
  }, []);
  
  return(
    <View style={styles.container}>
      <AuthForm 
        title="Create an Account"
        buttonText="Sign up" 
        callback={signup}
      />
      <View style={styles.link}>
        <LinkButton 
          text={'Sign in instead'}
          callback={() => navigation.navigate('SigninScreen')}
        />
      </View>
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 80,
    alignContent: 'center',
    marginHorizontal: 40,
  },
  link: {
    marginTop: 10,
    marginHorizontal: 10,
  },
});



export default SignupScreen;