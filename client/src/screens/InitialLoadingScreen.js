import { useContext, useEffect } from 'react';
import AuthContext from '../contexts/AuthContext';



function InitialLoadingScreen() {
  const { tryLocalSignin } = useContext(AuthContext);
  useEffect(() => {
    tryLocalSignin()
  }, []);
  return null;
}



export default InitialLoadingScreen;