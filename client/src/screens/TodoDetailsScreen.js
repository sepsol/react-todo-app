import React, { useContext, useState } from 'react';
import { Text, TextInput, Button, StyleSheet, View } from 'react-native';
import TodoContext from '../contexts/TodoContext';
import { Textarea } from 'native-base';



function TodoDetailsScreen({ navigation, route }) {
  const id = route.params;
  const { todosState, editTodo } = useContext(TodoContext);
  const todo = todosState.find(item => item._id === id);
  // console.log(todosState)
  // const [ title, setTitle ] = useState(todo.title);
  
  return(
    <View>
      {/* <TextInput autoFocus value={title} onChangeText={setTitle} /> */}
      {/* <Textarea></Textarea> */}

      {/* BUG: There's a problem with the last 2 buttons. */}
      <Button title="Get todos state" onPress={() => console.log(todosState)}/>
      <Button title="Get todo" onPress={() => console.log(todo)}/>
      <Button title="Get todo title" onPress={() => console.log(todo.title)}/>
    </View>
  );
}


const styles = StyleSheet.create({});



export default TodoDetailsScreen;