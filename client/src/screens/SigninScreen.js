import React, { useContext, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';

import AuthContext from '../contexts/AuthContext';

import AuthForm from '../components/AuthForm';
import LinkButton from '../components/LinkButton';



function SigninScreen({ navigation }) {
  const { clearErrorMessage, signin } = useContext(AuthContext);
  
  useEffect(() => {
    const listener = navigation.addListener('blur', () => clearErrorMessage());
    return listener;
  }, []);
  
  return(
    <View style={styles.container}>
      <AuthForm 
        title="Sign In to Todos"
        buttonText="Sign in" 
        callback={signin}
      />
      <View style={styles.link}>
        <LinkButton 
          text={'Create an account'}
          callback={() => navigation.navigate('SignupScreen')}
        />
      </View>
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 80,
    alignContent: 'center',
    marginHorizontal: 40,
  },
  link: {
    marginTop: 10,
    marginHorizontal: 10,
  },
});



export default SigninScreen;