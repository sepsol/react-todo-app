import React, { useReducer, useContext } from 'react';
import { AsyncStorage } from 'react-native';
import server from '../api/server';



const AuthContext = React.createContext();


function authReducer(prevState, action) {
  switch (action.type) {
    case 'clear_error':
      return { ...prevState, errorMessage: '' };
    case 'add_error':
      return {...prevState, errorMessage: action.payload };
    case 'signin':
      return { token: action.payload, errorMessage: '', isLoading: false };
    case 'signout':
      return { token: null, errorMessage: '', isLoading: false }
    default:
      return prevState;
  }
}



export function AuthProvider({ children }) {
  const [authState, dispatch] = useReducer(authReducer, { token: null, errorMessage: '', isLoading: true });
  
  
  function clearErrorMessage() {
    dispatch({ type: 'clear_error' });
  }
  
  async function tryLocalSignin() {
    try {
      let localToken = await AsyncStorage.getItem('token');
      if (localToken) {
        dispatch({ type: 'signin', payload: localToken });
      } else {
        dispatch({ type: 'signout' });
      }
    } catch (err) {
      console.log('Failed to retrieve the local token.');
    }
  }

  async function signup({ username, password }) {
    try {
      const response = await server.post('/signup', { username, password });
      await AsyncStorage.setItem('token', response.data.token);
      dispatch({ type: 'signin', payload: response.data.token });
    } catch (err) {
      dispatch({ type: 'add_error', payload: "Something went wrong with sign up." });
    }
  }

  async function signin({ username, password }) {
    try {
      const response = await server.post('/signin', { username, password });
      await AsyncStorage.setItem('token', response.data.token);
      dispatch({ type: 'signin', payload: response.data.token });
    } catch (err) {
      dispatch({ type: 'add_error', payload: "We couldn't sign you in." });
    }
  }

  async function signout() {
    try {
      await AsyncStorage.removeItem('token');
      dispatch({ type: 'signout' });
    } catch (err) {
      console.log('Failed to remove the local token and sign out.');
    }
  }
  


  return(
    <AuthContext.Provider 
      value={{ authState, clearErrorMessage, signup, signin, tryLocalSignin, signout }}
    >
      {children}
    </AuthContext.Provider>
  );
}



export default AuthContext;