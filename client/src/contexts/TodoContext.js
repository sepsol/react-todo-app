import React, { useReducer } from 'react';
import server from '../api/server';



const TodoContext = React.createContext();



function todosReducer(prevState, action) {
  switch (action.type) {
    case 'fetch_todos':
      return action.payload;
    case 'add_todo':
      return [
        ...prevState,
        { title: action.payload }
      ];
    case 'delete_todo':
      return prevState.filter(item => item._id !== action.payload);
    case 'clear_todos':
      return [];  // TODO
    case 'check_todo':
      return prevState.find(item => item._id === action.payload.id ? action.payload.checked : item.checked);
    default:
      return prevState;
  }
}



export function TodoProvider({ children }) {
  const [todosState, dispatch] = useReducer(todosReducer, []);

  
  async function fetchTodos() {
    try {
      const response = await server.get('/mytodos');
      dispatch({ type: 'fetch_todos', payload: response.data });
    } catch (err) {
      console.log(`Failed fetching data from the server.\n${err}`);
    }
  }

  async function addTodo(title) {
    if (!title) return alert("You must enter some title for your todo.");
    // dispatch({ type: 'add_todo', payload: title });
    try {
      await server.post('/mytodos', { title });
    } catch (err) {
      alert(`Couldn't add your todo.\n${err}`);
    }
  }

  async function editTodo(id, newTitle){
    await server.put(`/mytodos/${id}/edit`, { title: newTitle });
  }

  async function deleteTodo(id) {
    // dispatch({ type: 'delete_todo', payload: id });
    await server.delete(`/mytodos/${id}`);
  }

  // FIX: when a user logs out and another one loggs in, the previous todos are visible for a split second
  function clearTodos() {
    dispatch({ type: 'clear_todos' });
  }

  async function checkTodo(id, checked) {
    // dispatch({ type: 'check_todo', payload: { id, checked } });
    await server.put(`/mytodos/${id}`, { checked });
  }
  
  
  return(
    <TodoContext.Provider 
      value={{ todosState, fetchTodos, addTodo, editTodo, deleteTodo, clearTodos, checkTodo }}
    >
      {children}
    </TodoContext.Provider>
  );
}



export default TodoContext;