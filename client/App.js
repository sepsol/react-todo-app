import React, { /* useEffect */ } from 'react';
import { TodoProvider } from './src/contexts/TodoContext';
import { AuthProvider } from './src/contexts/AuthContext';
import Navigation from './src/navigators/NavigationContainer';
// import * as Font from 'expo-font';



function App() {
  // Failed to load native-base's fonts
  // useEffect(() => {
  //   (async function fetchFonts() {
  //     await Font.loadAsync({
  //       Roboto_medium: require('./assets/fonts/Roboto-Medium.ttf'),
  //     })
  //   })()
  // },[]);
  return(
    <TodoProvider>
      <AuthProvider>
        <Navigation />
      </AuthProvider>
    </TodoProvider>
  );
}



export default App;